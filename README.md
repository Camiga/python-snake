# Python Snake

A simple snake game written in Python using the pygame library. This project roughly follows this guide: https://youtu.be/CD4qAhfFuLo

# Credits
Music from https://filmmusic.io
"Pixelland" by Kevin MacLeod (https://incompetech.com)
License: CC BY (http://creativecommons.org/licenses/by/4.0/)

