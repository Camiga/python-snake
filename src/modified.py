# Python Snake Game.

import sys
import math
import random
import pygame
import tkinter as tk
from tkinter import messagebox

# Color scheme settings.
snake_color = (0, 50, 255)
eye_color = (255, 255, 255)
snack_color = (255, 50, 50)
line_color = (0, 255, 0)
background_color = (0, 125, 0)
text_color = (0, 200, 0)
pupil_color = (51, 51, 51)
mouth_color = (175, 0, 0)

# Size settings.
rows = 20
width = 600

# Initialize text to be drawn on the background.
pygame.font.init()
# Use Arial font, and determine font size from window size.
font = pygame.font.SysFont('Arial', width // 5)
# Pieces of the text to be drawn.
title = font.render('Snah-Kee!', True, text_color)
desc1 = font.render('(The Bootleg', True, text_color)
desc2 = font.render('Snake)', True, text_color)

# Initialize sound system.
pygame.mixer.init()
# Load in some sound effects.
loseSound = pygame.mixer.Sound('sfx/lose.wav')
collectSound = pygame.mixer.Sound('sfx/collect.wav')
# Load background music.
pygame.mixer.music.load('sfx/music.wav')
'''
Music from https://filmmusic.io
"Pixelland" by Kevin MacLeod (https://incompetech.com)
License: CC BY (http://creativecommons.org/licenses/by/4.0/)
'''


class cube(object):
    def __init__(self, start, dirnx=1, dirny=0, color=snake_color):
        self.pos = start
        self.dirnx = 1
        self.dirny = 0
        self.color = color

    def move(self, dirnx, dirny):
        self.dirnx = dirnx
        self.dirny = dirny
        self.pos = (self.pos[0] + self.dirnx, self.pos[1] + self.dirny)

    def draw(self, surface, face=False, isSnack=False):
        dis = width // rows
        i = self.pos[0]
        j = self.pos[1]

        # If the item being drawn is a snack, draw a small cube.
        if isSnack:
            pygame.draw.rect(
                surface,
                self.color,
                (i * dis + 7.5,
                 j * dis + 7.5,
                 dis - 12,
                 dis - 12))
        # Or draw a cube that fills the entire grid.
        else:
            pygame.draw.rect(
                surface,
                self.color,
                (i * dis + 1,
                 j * dis + 1,
                 dis,
                 dis))

        if face:
            centre = dis // 2
            radius = dis // 10

            # Draw the white part of the eyes.
            circleMiddle = (i * dis + centre - radius, j * dis + 8)
            circleMiddle2 = (i * dis + dis - radius * 2, j * dis + 8)
            pygame.draw.circle(surface, eye_color, circleMiddle, radius)
            pygame.draw.circle(surface, eye_color, circleMiddle2, radius)

            # Draw pupils on the eyes.
            pupilCircle = (i * dis + dis - radius, j * dis + 8)
            pupilCircle2 = (i * dis + dis - radius * 5, j * dis + 8)
            pygame.draw.circle(surface, pupil_color, pupilCircle, radius - 1)
            pygame.draw.circle(surface, pupil_color, pupilCircle2, radius - 1)

            # Draw a mouth.
            mouth = (
                i * dis + centre,
                j * dis + centre,
                (width // rows) * 0.35,
                (width // rows) * 0.25)
            pygame.draw.rect(surface, mouth_color, mouth, (width // rows) // 3)


class snake(object):
    body = []
    turns = {}

    def __init__(self, color, pos):
        self.color = color
        self.head = cube(pos)
        self.body.append(self.head)
        # Both of these values are 0 to make snake sit still at the beginning.
        self.dirnx = 0
        self.dirny = 0

    def move(self, canMove):
        # Always update the dirction of the snake.
        for event in pygame.event.get():
            # Please note that the MOUSEMOTION event has been blocked in main()
            if event.type == pygame.QUIT:
                pygame.quit()
                # Add a sys.exit() to silence an error.
                sys.exit(0)

            keys = pygame.key.get_pressed()

            for key in keys:
                if keys[pygame.K_LEFT]:
                    self.dirnx = -1
                    self.dirny = 0

                elif keys[pygame.K_RIGHT]:
                    self.dirnx = 1
                    self.dirny = 0

                elif keys[pygame.K_UP]:
                    self.dirnx = 0
                    self.dirny = -1

                elif keys[pygame.K_DOWN]:
                    self.dirnx = 0
                    self.dirny = 1

                # Update the direction of the snake on every key press.
                self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]

        # Only move the snake if the move_cycle variable has hit 1.
        if canMove:
            for i, c in enumerate(self.body):
                p = c.pos[:]
                if p in self.turns:
                    turn = self.turns[p]
                    c.move(turn[0], turn[1])
                    if i == len(self.body) - 1:
                        self.turns.pop(p)
                else:
                    # Removed the chunk of code that teleports the snake to the
                    # other side.
                    c.move(c.dirnx, c.dirny)

    def reset(self, pos):
        self.head = cube(pos)
        self.body = []
        self.body.append(self.head)
        self.turns = {}
        # Set these both to 0 for the snake to sit still on restart.
        self.dirnx = 0
        self.dirny = 0
        # Post an empty event to update the snake movement to 0.
        pygame.event.post(pygame.event.Event(pygame.USEREVENT))

    def addCube(self):
        tail = self.body[-1]
        dx, dy = tail.dirnx, tail.dirny

        if dx == 1 and dy == 0:
            self.body.append(cube((tail.pos[0] - 1, tail.pos[1])))
        elif dx == -1 and dy == 0:
            self.body.append(cube((tail.pos[0] + 1, tail.pos[1])))
        elif dx == 0 and dy == 1:
            self.body.append(cube((tail.pos[0], tail.pos[1] - 1)))
        elif dx == 0 and dy == -1:
            self.body.append(cube((tail.pos[0], tail.pos[1] + 1)))

        self.body[-1].dirnx = dx
        self.body[-1].dirny = dy

    def draw(self, surface):
        for i, c in enumerate(self.body):
            if i == 0:
                c.draw(surface, True)
            else:
                c.draw(surface)


def drawGrid(surface):
    sizeBtwn = width // rows

    x = 0
    y = 0

    for l in range(rows):
        x += sizeBtwn
        y += sizeBtwn

        # Draw lines with 10 pixels cut off on each side + 2px thickness.
        pygame.draw.line(surface, line_color, (x, 10), (x, width - 10), 2)
        pygame.draw.line(surface, line_color, (10, y), (width - 10, y), 2)


def redrawWindow(surface, inputSnake, inputSnack):
    surface.fill(background_color)

    # Fill the three parts of the text on the background.
    surface.blit(title, (width / 7, width / 4))
    surface.blit(desc1, (width / 14, width / 2.5))
    surface.blit(desc2, (width / 3.8, width / 1.8))

    # Put this after the text, so the snake draws over it.
    inputSnake.draw(surface)

    # For snacks, draw with "face" false and "isSnack" true.
    inputSnack.draw(surface, False, True)
    drawGrid(surface)
    pygame.display.update()


def randomSnack(item):
    positions = item.body

    while True:
        x = random.randrange(rows)
        y = random.randrange(rows)
        if len(list(filter(lambda z: z.pos == (x, y), positions))) > 0:
            continue
        else:
            break

    return (x, y)


def message_box(subject, content):
    root = tk.Tk()
    root.attributes("-topmost", True)
    root.withdraw()
    messagebox.showinfo(subject, content)
    try:
        root.destroy()
    except BaseException:
        pass

# Function to trigger a game over.


def game_over(current_score, current_snake):
    # Pause the background music from playing.
    pygame.mixer.music.pause()
    # Play a game over sound.
    loseSound.play()
    # Display a game over message.
    message_box('Game over!', 'Your score was ' +
                current_score + '. Press OK to play again.')
    # Once the box is closed, resume music and restart game.
    pygame.mixer.music.unpause()
    current_snake.reset((rows // 2, rows // 2))

# Snake game is started here, after all the functions and variables.


def main():
    # Loop background music forever using -1.
    pygame.mixer.music.play(-1)

    win = pygame.display.set_mode((width, width))
    s = snake(snake_color, (rows // 2, rows // 2))
    snack = cube(randomSnack(s), color=snack_color)
    clock = pygame.time.Clock()

    # Disable mouse movement and window active events to fix a bug.
    # The game currently does not need to monitor these events.
    pygame.event.set_blocked(pygame.MOUSEMOTION)
    pygame.event.set_blocked(pygame.ACTIVEEVENT)

    # Post a blank event to update the snake right at the beginning.
    # This allows the snake to initially sit still.
    pygame.event.post(pygame.event.Event(pygame.USEREVENT))

    # Variable used to slow the snake down, since the framerate was
    # increased to 60fps, making the snake much faster.
    move_cycle = 0

    while True:
        # Set the game to run at 60fps.
        # This makes keyboard input more reliable.
        clock.tick(60)

        # Update the move_cycle variable until it has hit a limit.
        move_cycle += 1
        # Make this number larger to slow snake down.
        if move_cycle >= 7:
            # Update the snakes direction, and move the snake.
            s.move(True)
            # Reset the move cycle.
            move_cycle = 0
        else:
            # If we're still counting, update the snake direction anyways, but
            # don't move the snake.
            s.move(False)

        if s.body[0].pos == snack.pos:
            # Play a sound effect for collecting snack.
            collectSound.play()
            s.addCube()
            snack = cube(randomSnack(s), color=snack_color)

        for x in range(len(s.body)):
            # Score is the length of the snake minus one.
            # The reason one is subtracted is because we don't count the head
            # of the snake as part of the score.
            score = str(len(s.body) - 1)
            pygame.display.set_caption(
                'Snah-Kee! (The Bootleg Snake) | Score: ' + score)

            if s.body[x].pos in list(map(lambda z: z.pos, s.body[x + 1:])):
                # Game over, providing the score and snake variable.
                game_over(score, s)
                break

        # Code to detect if you have run into a wall.
        # Get the X and Y of the head of the snake.
        head_x = s.head.pos[0]
        head_y = s.head.pos[1]

        # Game over if the snake is outside of the game rows.
        if head_x >= rows or head_y >= rows:
            game_over(score, s)
        # Game over if the snake is at a negative number.
        if head_x < 0 or head_y < 0:
            game_over(score, s)

        # Redraw the window, by providing the pygame window, the snake, and the
        # snack location.
        redrawWindow(win, s, snack)
    pass


main()
